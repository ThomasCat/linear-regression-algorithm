/*
    Linear Regression Algorithm
    Copyright (C) 2019  Owais Shaikh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cstring>
#define MAX 10
double offset = 0.01;

int main(int argc, char const *argv[]){
  double x[MAX];
  double y[MAX];
  int arraySize;
  std::cout<<"\n________________\n";
  std::cout<<"ARRAY";
  std::cout<<"\n________________\n\n";
  std::cout<<"Enter array size: ";
  std::cin>>arraySize;
  std::cout<<"\n________________\n";
  std::cout<<"VALUES FOR X";
  std::cout<<"\n________________\n\n";
  for (int readCount=1; readCount<=arraySize; readCount++){
    std::cout<<"Enter value #"<<readCount<<" for X: ";
    std::cin>>x[readCount];
  }
  std::cout<<"\n________________\n";
  std::cout<<"VALUES FOR Y";
  std::cout<<"\n________________\n\n";
  for (int readCount=1; readCount<=arraySize; readCount++){
    std::cout<<"Enter value #"<<readCount<<" for Y: ";
    std::cin>>y[readCount];
  }
  //  test array
  //  double x[] = {1, 2, 3, 4, 5};
  //  double y[] = {5, 4, 3, 2, 1};
  double nodeA = 0;
  double nodeB = 0;
  std::cout<<"\n________________\n\n";
  for(int count=1; count<MAX; count++){
    int mod=count%5;
    double p=nodeA+nodeB*x[mod];
    double error=p-y[mod];
    nodeA=nodeA-offset*error;
    nodeB=nodeB-offset*error*x[mod];
    std::cout<<"Node A: ";
    std::cout<<nodeA;
    std::cout<<"  Node B: ";
    std::cout<<nodeB;
    std::cout<<"  Error: ";
    std::cout<<error;
    std::cout<<"\n";
  }    
}